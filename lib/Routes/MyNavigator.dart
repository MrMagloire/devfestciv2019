import 'package:devfestciv2019/tools/AssetTools.dart';
import 'package:devfestciv2019/views/home.dart';
import 'package:flutter/material.dart';

class MyNavigator {
	static void goToHome(BuildContext context) {
		Navigator
		.of(context)
		.pushReplacement(
			MaterialPageRoute(
				settings: RouteSettings(name: AssetTools.rtHome),
				builder: (BuildContext context) {
					return Home();
				}
			)
		);
	}
}