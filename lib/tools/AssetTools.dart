class AssetTools {
	// Images
	static const String imageDir = "assets/img/";
    static const String imgBanner = imageDir+"banner_light.png";

	// Routes
    static const String rtSplash = "/splash";
    static const String rtHome = "/home";
}