import 'package:devfestciv2019/tools/AssetTools.dart';
import 'package:devfestciv2019/views/SplashScreen.dart';
import 'package:devfestciv2019/views/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MyApp extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		// Device Orientation
		SystemChrome.setPreferredOrientations([
			DeviceOrientation.portraitUp,
		]);
		return MaterialApp(
			title: 'DevFestCiv2019',
			theme: new ThemeData(
                primaryColor: Colors.white,
		  	),
			home: new SplashScreen(),
			routes: <String, WidgetBuilder>{
				AssetTools.rtSplash : (BuildContext context) => new SplashScreen(),
				AssetTools.rtHome: (BuildContext context) => new Home(),
			}
		);
	}
}