import 'package:devfestciv2019/Routes/MyNavigator.dart';
import 'package:devfestciv2019/tools/AssetTools.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
	@override
	_SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

	@override
	initState(){
		super.initState();

		Future.delayed(Duration(seconds: 3),(){
			MyNavigator.goToHome(context);
		});
	}

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			body: Container(
				height: MediaQuery.of(context).size.height - 75.0,
				alignment: Alignment.center,
				child: Image.asset(AssetTools.imgBanner,),
			),
		);
	}
}