import 'package:devfestciv2019/tools/AssetTools.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
	@override
	_HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			body: SingleChildScrollView(
				child: Container(
					height: MediaQuery.of(context).size.height,
					child: ListView(
						children: <Widget>[
							Container(
								margin: EdgeInsets.only(top: 20.0),
								height: 50.0,
								alignment: Alignment.center,
								child: Text(
									"Bienvenu(e) au",
									style: TextStyle(fontSize: 25.0, color: Colors.blue, fontWeight: FontWeight.bold),
									textAlign: TextAlign.center,
								)
							),
							Container(
								decoration: BoxDecoration(
									border: Border.all(
										color: Colors.grey
									),
									borderRadius: BorderRadius.circular(10.0),
								),
								margin: EdgeInsets.symmetric(horizontal: 25.0),
								padding: EdgeInsets.all(6.0),
								child: Image.asset(AssetTools.imgBanner),
							),
							Container(
								margin: EdgeInsets.only(top: 5.0),
								height: 50.0,
								alignment: Alignment.center,
								child: Text(
									"#DevFestCiV19",
									style: TextStyle(fontSize: 25.0, color: Colors.blue, fontWeight: FontWeight.bold),
									textAlign: TextAlign.center,
								)
							),
						],
					),
				),
			),
		);
	}
}